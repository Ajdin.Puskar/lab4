package datastructure;


import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int columns;
    CellState[][] cellState;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        cellState = new CellState[rows][columns];
        for (int row=0;row<rows;row++) {
            for (int column=0;column<columns;column++) {
                cellState[row][column] = initialState;

            }
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        cellState[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return cellState[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copied = new CellGrid(this.rows,this.columns,CellState.DEAD);
        for (int row=0; row<cellState.length;row++) {
            for (int col=0;col<cellState[row].length;col++){
                copied.set(row, col, cellState[row][col]);
            }
        }
        return copied;
    }

}
